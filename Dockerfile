ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:2020-ss.1
FROM ${BASE_IMAGE}

MAINTAINER Max Vitz, maximilian.vitz@rwth-aachen.de, 2021
USER root

# Install all OS dependencies for notebook server that starts but lacks all
# features (e.g., download as all possible file formats)
RUN apt-get update && \
    apt-get install -y vim nano wget rsync dpkg-dev cmake libpng-dev libjpeg-dev \
     libgs-dev g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev libssl-dev
RUN conda update -n base conda


# Install packages via requirements.txt
RUN mkdir -p /install
ADD requirements.txt /install/requirements.txt
RUN pip3 install --no-cache-dir -r /install/requirements.txt -I

# from example profile:
# .. Or update conda base environment to match specifications in environment.yml
ADD environment.yml /tmp/environment.yml


# All packages specified in environment.yml are installed in the base environment
RUN conda env update -f /tmp/environment.yml && \
    conda clean -a -f -y
RUN conda install root -c conda-forge

# RUN conda activate base
# end example profile
# Updated pipeline
